# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import random

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy, ugettext as _

from registration.models import UserActivation

class RegistrationForm(forms.Form):
    """ Form for user registration """
    username = forms.RegexField(max_length=30, regex=r'^\w+$',
                               label=ugettext_lazy(u'Choose a username:'),
                               help_text=ugettext_lazy(u'May contain only letters, numbers, underscores or hyphens'))
    email = forms.EmailField(label=ugettext_lazy(u'Email:'))
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label=ugettext_lazy(u'Password:'), required=False, min_length=6,
                                help_text=ugettext_lazy(u'At least 6 characters'))
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label=ugettext_lazy(u'Confirm password:'), required=False)

    def clean_username(self):
        """  Validate the username (correctness and uniqueness)"""
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_(u'This username is already taken. Please choose another.'))

    def clean(self):
        cleaned_data = self.cleaned_data
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if not password1:
            raise forms.ValidationError(_(u'You must provide a password'))

        if password1 and password1 != password2:
            raise forms.ValidationError(_(u'The passwords do not match'))
        return cleaned_data

    def save(self):
        """ Create the user """
        username = self.cleaned_data['username']
        email = self.cleaned_data['email']
        password = self.cleaned_data['password1']

        new_user = User.objects.create_user(username=username,
                           email=email,
                           password=password)
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        activation_key = hashlib.sha1(salt+username).hexdigest()
        new_user.is_active = False
        new_user.save()
        user_act = UserActivation.objects.create(user=new_user, activation_key=activation_key)
        # Send activation email
        current_site = Site.objects.get_current()
        subject = settings.EMAIL_SUBJECT_PREFIX + _(u'Account activation')
        # FIXME: this should go in a template...
        message = _(u"This is a confirmation that your registration on %s succeeded. To activate your account, please click on the link below or copy and paste it in a browser.") % current_site.name
        message += "\n\nhttp://%s%s\n\n" % (current_site.domain, str(reverse("activation", kwargs={'key': activation_key})))
        message += _(u"Administrators of %s" % current_site.name)

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL,
                  (email,), fail_silently=False)

        return new_user

