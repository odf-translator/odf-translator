# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic import FormView, RedirectView

from registration.forms import RegistrationForm
from registration.models import UserActivation

class SiteRegister(FormView):
    form_class = RegistrationForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        form.save()
        return super(SiteRegister, self).form_valid(form)

    def get_success_url(self):
            return reverse('register_success')

class AccountActivation(RedirectView):
    """ Activate an account through the link a requestor has received by email """
    permanent = False

    def get_redirect_url(self, **kwargs):
        if kwargs.get('success', False):
            return reverse('login')
        else:
            return reverse('home')

    def get(self, request, *args, **kwargs):
        try:
            act = UserActivation.objects.get(activation_key=kwargs['key'])
            act.activate()
            messages.success(request, "Your account has been activated.")
            kwargs['success'] = True
        except UserActivation.DoesNotExist:
            messages.error(request, "Sorry, the key you provided is not valid or you have already activated it.")
        return super(AccountActivation, self).get(request, *args, **kwargs)
