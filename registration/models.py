from django.contrib.auth.models import User
from django.db import models

class UserActivation(models.Model):
    user           = models.OneToOneField(User)
    activation_key = models.CharField(max_length=40)

    def activate(self):
        self.user.is_active = True
        self.user.save()
        self.delete()

