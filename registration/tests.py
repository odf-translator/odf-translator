# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase

from registration.models import UserActivation

class RegistrationTest(TestCase):
    def test_register(self):
        response = self.client.post(reverse('register'),
            {'username':'newuser', 'email': 'newuser@example.org',
             'password1': 'blah012', 'password2': 'blah012'})
        user = UserActivation.objects.get(user__username='newuser')
        self.assertFalse(user.user.is_active)
        response = self.client.get(reverse('activation', args=[user.activation_key]))
        user = User.objects.get(username='newuser')
        self.assertTrue(user.is_active)

