import os
import site
import sys

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PARENT_ROOT  = os.path.dirname(PROJECT_ROOT)
PROJECT_NAME = PROJECT_ROOT.split('/')[-1]
if PROJECT_ROOT not in sys.path:
     sys.path.append(PARENT_ROOT)
     sys.path.append(PROJECT_ROOT)
virtual_env_packs = "%s/virtualenvs/%s/lib/python%d.%d/site-packages" % (
    PARENT_ROOT, PROJECT_NAME, sys.version_info[0], sys.version_info[1])
if os.path.exists(virtual_env_packs):
    site.addsitedir(virtual_env_packs)
    # Put virtualenv path at the top of the path
    sys.path.remove(virtual_env_packs)
    sys.path.insert(0, virtual_env_packs)

os.environ['DJANGO_SETTINGS_MODULE'] = '%s.settings' % PROJECT_NAME

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
