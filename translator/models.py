# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
import os
import re
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.images import get_image_dimensions
from django.db import models
#from django.db.models.signals import post_save

from translator.fields import UuidField
from translator.storage import OverwriteFSStorage
from translator.utils import run_command

overwrite_fs = OverwriteFSStorage()

class Series(models.Model):
    uuid      = UuidField(auto=True, editable=False)
    title     = models.CharField(max_length=150)
    languages = models.ManyToManyField('Language')

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('series_detail', (), {'slug': self.uuid})

    def is_admin(self, user):
        if user.is_authenticated():
            return self.seriesperm_set.filter(user=user, admin=True).count() > 0
        return False

    def is_translator(self, user):
        if user.is_authenticated():
            return self.seriesperm_set.filter(user=user).count() > 0
        return False

class SeriesPerm(models.Model):
    user  = models.ForeignKey(User)
    obj   = models.ForeignKey(Series)
    admin = models.BooleanField(default=False)

    class Meta:
         unique_together = ("user", "obj")

    def __unicode__(self):
        return u"%s %s for %s" % (
            self.user.username,
            self.admin and "administrator" or "translator",
            self.obj.title)


def doc_upload_path(instance, filename):
    return os.path.join(instance.series.uuid, filename)

class Document(models.Model):
    series    = models.ForeignKey(Series)
    title     = models.CharField(max_length=150)
    original  = models.FileField(upload_to=doc_upload_path, storage=overwrite_fs, verbose_name="Document")
    up_time   = models.DateTimeField(editable=False)
    total_str = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title

    def set_upfile(self, uploaded):
        self.original = uploaded
        self.up_time = datetime.now()
        self.save()
        self.update_document()

    def original_fname(self):
        return os.path.basename(self.original.file.name)

    def translation(self, language):
        try:
            return self.translation_set.get(lang=language)
        except Translation.DoesNotExist:
            return None

    def trans_stats(self, language):
        """ Return doc trans statistics as a 4-tuple (trans, fuzzy, untrans, percentage) """
        trans = self.translation(language)
        if trans is None:
            stats = (0, 0, self.total_str, 0)
        else:
            total_str = trans.trans + trans.fuzzy + trans.untrans
            if total_str == 0:
                stats = (0, 0, 0, 0)
            else:
                stats = (trans.trans, trans.fuzzy, trans.untrans,
                    int(trans.trans/float(total_str)*100)
                )
        return stats

    def update_document(self):
        """ Generate a fresh pot file whenever a new document is uploaded,
            Then merge with latest po translation
            Then create translated document
        """
        base, ext = os.path.splitext(self.original_fname())
        series_path = os.path.join(settings.MEDIA_ROOT, self.series.uuid)
        pot_path = os.path.join(series_path, 'pot')
        if not os.path.exists(pot_path):
            os.mkdir(pot_path)
        # The 'cd' is important as it hides the absolute paths in itstool pot output
        run_command("cd %(spath)s && %(path)sitstool -o %(outpot)s %(original)s" % {
            'spath' : series_path,
            'path'  : settings.ITSTOOL_PATH,
            'outpot': os.path.join(pot_path, '%s.pot' % base),
            'original': self.original_fname(),
        }, raise_on_error=True)

        # Update images
        img_dir = os.path.join(pot_path, '%s__images' % base)
        existing = [di.pk for di in self.documentimage_set.all()]
        if os.path.exists(img_dir):
            for img in os.listdir(img_dir):
                if not "." in img:
                    # No extension (probably Object Replacement image)
                    continue
                doc_img, created = DocumentImage.objects.get_or_create(
                    doc=self, fname=img,
                    defaults={'doc':self, 'fname': img})
                if not created:
                    existing.remove(doc_img.pk)
        DocumentImage.objects.filter(pk__in=existing).delete()

        # Get total strings
        (status, output, errs) = run_command("LANG=C msgfmt --statistics -o /dev/null %s" % (
            os.path.join(pot_path, '%s.pot' % base),))
        r_un = re.search(r"([0-9]+) untranslated", errs)
        if r_un:
            self.total_str = int(r_un.group(1))
            self.save()

        # Merge
        for lang in self.series.languages.all():
            trans = self.translation(lang)
            if trans is None:
                continue
            trans.update_translation()


class Language(models.Model):
    code = models.SlugField(max_length=6, unique=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.code)


def po_upload_path(instance, filename):
    return os.path.join(instance.doc.series.uuid, 'po',
        datetime.now().strftime('%Y%m%d-%H%M'), filename)

class Translation(models.Model):
    doc     = models.ForeignKey(Document)
    lang    = models.ForeignKey(Language)
    up_time = models.DateTimeField(auto_now_add=True)
    up_file = models.FileField(upload_to=po_upload_path)
    merged  = models.CharField(max_length=150, null=True) # path to merged file
    trans   = models.SmallIntegerField(default=0)
    fuzzy   = models.SmallIntegerField(default=0)
    untrans = models.SmallIntegerField(default=0)
    reserved_by = models.ForeignKey(User, null=True)
    errors  = models.TextField(blank=True, default='')

    def __unicode__(self):
        return u"%s translation of document '%s'" % (lang.name, doc.title)

    def translated(self):
        """ Returns a path relative to media_root """
        base, ext = os.path.splitext(unicode(self.doc.original))
        path = "%s.%s%s" % (base, self.lang.code, ext)
        if os.path.exists(os.path.join(settings.MEDIA_ROOT, path)):
            return path
        else:
            return None

    def set_upfile(self, uploaded):
        self.up_file = uploaded
        self.up_time = datetime.now()
        self.save()
        self.update_translation()

    def update_translation(self):
        base, ext = os.path.splitext(self.doc.original_fname())
        pot_path = os.path.join(settings.MEDIA_ROOT, self.doc.series.uuid, 'pot')
        po_path = os.path.join(settings.MEDIA_ROOT, str(self.up_file))
        merged_rel_path =  u"%s.merged.po" % str(self.up_file)[:-3]
        run_command("msgmerge --previous -o %(new_po)s %(po)s %(pot)s" % {
            'new_po': os.path.join(settings.MEDIA_ROOT, merged_rel_path),
            'po':     po_path,
            'pot':    os.path.join(pot_path, '%s.pot' % base),
        }, raise_on_error=True)
        self.merged = merged_rel_path

        # Compute stats
        (status, output, errs) = run_command("LANG=C msgfmt --statistics -o /dev/null %s" % (
            os.path.join(settings.MEDIA_ROOT, merged_rel_path),))
        # msgfmt output stats on stderr
        r_tr = re.search(r"([0-9]+) translated", errs)
        r_un = re.search(r"([0-9]+) untranslated", errs)
        r_fz = re.search(r"([0-9]+) fuzzy", errs)
        if r_tr:
            self.trans = int(r_tr.group(1))
        self.untrans = 0
        if r_un:
            self.untrans = int(r_un.group(1))
        self.fuzzy = 0
        if r_fz:
            self.fuzzy = int(r_fz.group(1))
        self.save()

        # Create translated doc
        mo_path = os.path.join(settings.MEDIA_ROOT, "%s.mo" % merged_rel_path[:-3])
        run_command("msgfmt -o %(mo)s %(po)s" % {
            'po': os.path.join(settings.MEDIA_ROOT, merged_rel_path),
            'mo': mo_path,
        }, raise_on_error=True)
        base, ext = os.path.splitext(unicode(self.doc.original))
        final_path = os.path.join(settings.MEDIA_ROOT, "%s.%s%s" % (base, self.lang.code, ext))
        command = "%(path)sitstool -m %(mo)s -I %(img_dir)s -o %(final)s %(current)s" % {
            'path':    settings.ITSTOOL_PATH,
            'mo':      mo_path,
            'img_dir': os.path.join(settings.MEDIA_ROOT, self.doc.series.uuid, 'img'),
            'final':   final_path,
            'current': os.path.join(settings.MEDIA_ROOT, unicode(self.doc.original)),
        }
        try:
            run_command(command, raise_on_error=True)
        except OSError, e:
            self.errors = str(e)
            self.save()
            os.remove(final_path)
            return
        if self.errors:
            self.errors = ""
            self.save()


class DocumentImage(models.Model):
    doc     = models.ForeignKey(Document)
    fname   = models.CharField(max_length=100)

    def img_path(self):
        img_dir = "%s__images" % os.path.splitext(self.doc.original_fname())[0]
        return os.path.join(settings.MEDIA_ROOT, self.doc.series.uuid, 'pot', img_dir, self.fname)

    def img_url(self):
        img_dir = "%s__images" % os.path.splitext(self.doc.original_fname())[0]
        return os.path.join(settings.MEDIA_URL, self.doc.series.uuid, 'pot', img_dir, self.fname)

    def img_dimensions(self):
        if not hasattr(self, '_width') or not hasattr(self, '_height'):
            try:
                self._width, self._height = get_image_dimensions(self.img_path())
            except TypeError:
                return (0, 0)
        return (self._width, self._height)


def im_upload_path(instance, filename):
    return os.path.join(instance.image.doc.series.uuid, 'img', instance.image.fname)

class ImageTranslation(models.Model):
    image   = models.ForeignKey(DocumentImage)
    lang    = models.ForeignKey(Language)
    up_time = models.DateTimeField(auto_now_add=True)
    up_file = models.FileField(upload_to=im_upload_path)

    def set_upfile(self, uploaded):
        self.up_file = uploaded
        self.save()
        self.image.doc.translation(self.lang).update_translation()

    def img_path(self):
        return self.up_file.file.name

    def img_url(self):
        return self.up_file.url

    def img_dimensions(self):
        if not hasattr(self, '_width') or not hasattr(self, '_height'):
            try:
                self._width, self._height = get_image_dimensions(self.img_path())
            except TypeError:
                return (0, 0)
        return (self._width, self._height)

    @property
    def doc(self):
        return self.image.doc
