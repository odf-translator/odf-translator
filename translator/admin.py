from django.contrib import admin

from translator import models

class LanguageAdmin(admin.ModelAdmin):
    search_fields = ('name', 'code')

admin.site.register(models.Series)
admin.site.register(models.SeriesPerm)
admin.site.register(models.Document)
admin.site.register(models.Language, LanguageAdmin)
admin.site.register(models.Translation)
admin.site.register(models.DocumentImage)
admin.site.register(models.ImageTranslation)
