import os

from django import template
from django.conf import settings
from django.core.urlresolvers import reverse

register = template.Library()

@register.filter
def doc_original(doc):
    return """<a href="%(media)s%(path)s">%(fname)s</a>""" % {
        'media': settings.MEDIA_URL,
        'path':  doc.original,
        'fname': doc.original_fname(),
        }

@register.filter
def doc_translation(doc, language):
    trans = doc.translation(language)
    if trans is None or not trans.up_file:
        # POT file
        fdir, f = os.path.split(str(doc.original))
        base, ext = os.path.splitext(f)
        pot_path = os.path.join(fdir, 'pot', "%s.pot" % base)
        return """<a href="%(media)s%(path)s">%(fname)s</a> (0%%)""" % {
            'media': settings.MEDIA_URL,
            'path':  pot_path,
            'fname': os.path.basename(pot_path),
        }
    else:
        po_path = trans.merged or trans.up_file
        return """<a href="%(media)s%(path)s">%(fname)s</a>""" % {
            'media': settings.MEDIA_URL,
            'path':  po_path,
            'fname': os.path.basename(trans.up_file.file.name),
        }

@register.filter
def translated_doc(trans):
    if trans is None:
        return "-"
    trans_file_path = trans.translated()
    if trans_file_path is None:
        return "-"
    return """<a href="%(media)s%(path)s">%(fname)s</a>""" % {
        'media': settings.MEDIA_URL,
        'path':  trans_file_path,
        'fname': os.path.basename(trans_file_path),
    }

@register.filter
def as_thumbnail(img):
    """ Display img thumb + thumb of the matching translation,
        img parameter is a DocumentImage instance
    """
    def get_width_height(img):
        max_w, max_h = 90, 50
        w, h = img.img_dimensions()
        if w==0 or h==0:
            return "<em>no valid image</em>"
        rate_w, rate_h = max_w/float(w), max_h/float(h)
        rate = rate_w > rate_h and rate_h or rate_w
        return int(w*rate), int(h*rate)
    w, h = get_width_height(img)
    div = """<div class="thumbnail"><img src="%(url)s" height="%(h)s" width="%(w)s"></div>""" % {
        'url': img.img_url(),
        'h': h, 'w': w,
    }
    trans = img.translation
    if trans:
        w, h = get_width_height(trans)
        next_div = """<div class="thumbnail"><img id="%(id)s" src="%(url)s" height="%(h)s" width="%(w)s"></div>""" % {
            'id' : "%d_trans" % img.pk,
            'url': trans.img_url(),
            'h': h, 'w': w,
        }
    else:
        next_div = """<div class="thumbnail"><img id="%(id)s" src="%(static)simg/cross.png" height="40"></div>""" % {
            'static': settings.STATIC_URL,
            'id':     "%d_trans" % img.pk,
        }
    return div + next_div

@register.filter
def cross_dim(img):
    """ Set cross dimensions depending on image """
    w, h = img.img_dimensions()
    if w > h: size = w
    else: size = h
    if size > 290: size = 290
    return 'height="%d"' % size

@register.filter
def show_stats(doc, lang):
    """ Show document statistics """
    trans, fuzzy, untrans, perc = doc.trans_stats(lang)
    total_str = trans + fuzzy + untrans
    if total_str == 0:
        return u"<i>no strings</i>"
    stats = u"(%d/%d/%d) %d%%" % (trans, fuzzy, untrans, perc)
    if perc == 100:
        stats = u'<span class="complete">%s</span>' % stats
    return stats

@register.filter
def reserved_by(doc, lang):
    trans = doc.translation(lang)
    if trans and trans.reserved_by:
        return u'<span class="star" title="%s is currently working on it.">*</span>' % trans.reserved_by
    return ""

@register.filter
def disp_user(user):
    if user.first_name or user.last_name:
        return u"%s (%s)" % (" ".join([user.first_name, user.last_name]), user.username)
    else:
        return user.username
