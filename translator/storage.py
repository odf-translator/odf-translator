import os
from django.core.files.storage import FileSystemStorage

class OverwriteFSStorage(FileSystemStorage):
    def get_available_name(self, name):
        """ Overwrite any existing file with same name """
        full_path = self.path(name)
        if os.path.exists(full_path):
            # Rename existing to .old
            os.rename(full_path, full_path + ".old")
        return name
