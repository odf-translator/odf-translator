# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
import os
from django import forms
from django.contrib.auth.models import User
from django.db import IntegrityError

from translator.models import Series, SeriesPerm, Document, Language
from translator.utils import run_command

class SeriesForm(forms.ModelForm):
    class Meta:
        model = Series
        fields = ('title',)
    first_doc  = forms.FileField(label="First document")

    def save(self):
        super(SeriesForm, self).save()
        doc = Document.objects.create(
            series  = self.instance,
            title   = self.cleaned_data['first_doc'].name,
            original= self.cleaned_data['first_doc'],
            up_time = datetime.now(),
        )
        doc.update_document()
        return self.instance

class AddLanguageForm(forms.Form):
    language = forms.ModelChoiceField(queryset=Language.objects.all().order_by('name'), empty_label=None)

class SeriesPermsForm(forms.Form):
    admin_username = forms.CharField(required=False, widget=forms.TextInput({ "placeholder": "username" }))
    trans_username = forms.CharField(required=False, widget=forms.TextInput({ "placeholder": "username" }))

    def save(self, series):
        try:
            if self.cleaned_data['admin_username']:
                SeriesPerm.objects.create(user=self.admin_username, obj=series, admin=True)
            if self.cleaned_data['trans_username']:
                SeriesPerm.objects.create(user=self.trans_username, obj=series, admin=False)
        except IntegrityError:
            pass

    def check_username(self, field_name):
        if self.cleaned_data[field_name]:
            try:
                setattr(self, field_name, User.objects.get(username=self.cleaned_data[field_name]))
            except User.DoesNotExist:
                raise forms.ValidationError("Username '%s' does not exist in the database." % self.cleaned_data[field_name])
        return self.cleaned_data[field_name]

    def clean_admin_username(self):
        return self.check_username('admin_username')

    def clean_trans_username(self):
        return self.check_username('trans_username')


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('series', 'original')
        widgets = {
            'series': forms.HiddenInput,
        }
    # title as hidden field ? (initially same as filename)

    def save(self):
        self.instance.up_time = datetime.now()
        self.instance.title = self.cleaned_data['original'].name
        doc = super(DocumentForm, self).save()
        doc.update_document()
        return doc

class UploadForm(forms.Form):
    """ File upload form (for different models, model has to define set_upfile method) """
    up_file = forms.FileField()

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance', None)
        super(UploadForm, self).__init__(*args, **kwargs)

    def save(self):
        self.instance.set_upfile(self.cleaned_data['up_file'])
        return self.instance

class PoUploadForm(UploadForm):
    def clean_up_file(self):
        data = self.cleaned_data['up_file']
        if data:
            ext = os.path.splitext(data.name)[1]
            if ext != '.po':
                raise forms.ValidationError("Only files with extension .po are admitted.")
            (status, out, err) = run_command("msgfmt -v --check-format -o /dev/null %s" % data.temporary_file_path())
            if status!=0 and err:
                raise forms.ValidationError(".po file does not pass 'msgfmt -vc'. Please correct the file and try again.")
        return data
