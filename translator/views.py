# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.views import generic

from translator.models import Series, SeriesPerm, Document, Translation, Language, DocumentImage, ImageTranslation
from translator import forms
from translator.templatetags.translator import as_thumbnail

class IndexView(generic.TemplateView):
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['series'] = Series.objects.filter(seriesperm__user=self.request.user)
        return context

class SeriesCreate(generic.CreateView):
    model = Series
    form_class = forms.SeriesForm

    def form_valid(self, form):
        response = super(SeriesCreate, self).form_valid(form)
        # Set current user as series admin
        SeriesPerm.objects.create(user=self.request.user, obj=form.instance, admin=True)
        return response

class SeriesEdit(generic.UpdateView):
    model = Series

class SeriesDetail(generic.DetailView):
    model = Series
    slug_field = 'uuid'
    context_object_name = 'series'

    def get_context_data(self, **kwargs):
        context = super(SeriesDetail, self).get_context_data(**kwargs)
        context['is_admin'] = self.object.is_admin(self.request.user)
        context['documents'] = self.object.document_set.all()
        context['languages'] = self.object.languages.all()
        return context

class SeriesPermsView(generic.FormView, generic.detail.SingleObjectMixin):
    model = Series
    slug_field = 'uuid'
    context_object_name = 'series'
    form_class = forms.SeriesPermsForm
    template_name = "translator/series_perms.html"

    def form_valid(self, form):
        self.object = self.get_object()
        form.save(self.object)
        return super(SeriesPermsView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SeriesPermsView, self).get_context_data(**kwargs)
        if not hasattr(self, 'object'):
            self.object = self.get_object()
        context['series'] = self.object
        admins, translators = [], []
        for perm in self.object.seriesperm_set.all():
            if perm.admin:
                admins.append(perm.user)
            else:
                translators.append(perm.user)
        context['admins'] = admins
        context['translators'] = translators
        context['is_admin'] = self.object.is_admin(self.request.user)
        return context

    def get_success_url(self):
        return self.request.path

class LanguageAdd(generic.FormView):
    form_class = forms.AddLanguageForm
    template_name = "translator/language_add.html"

    def get_form(self, form_class):
        self.series = Series.objects.get(uuid=self.kwargs['slug'])
        return super(LanguageAdd, self).get_form(form_class)

    def form_valid(self, form):
        self.series.languages.add(form.cleaned_data['language'])
        return super(LanguageAdd, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(LanguageAdd, self).get_context_data(**kwargs)
        context['series'] = self.series
        return context

    def get_success_url(self):
        return self.series.get_absolute_url()

class DocumentDetail(generic.DetailView):
    model = Document
    context_object_name = 'doc'

    def get_context_data(self, **kwargs):
        context = super(DocumentDetail, self).get_context_data(**kwargs)
        language = Language.objects.get(code=self.kwargs['code'])
        translation = self.object.translation(language)
        context.update({
            'series':   self.object.series,
            'language': language,
            'trans':    translation,
            'trans_pk': translation and translation.pk or language.code,
            'is_admin': self.object.series.is_admin(self.request.user),
            'is_translator': self.object.series.is_translator(self.request.user),
            'images':   self.object.documentimage_set.all(),
        })
        # Match image translations to images (only one query!)
        img_trans = dict((i.image_id, i) for i in ImageTranslation.objects.filter(lang=language, image__doc=self.object))
        for img in context['images']:
            img.translation = img_trans.get(img.id, None)
        return context

class DocumentCreate(generic.CreateView):
    model = Document
    form_class = forms.DocumentForm

    def get_initial(self):
        return {'series': Series.objects.get(uuid=self.kwargs['slug'])}

    def get_context_data(self, form):
        context = super(DocumentCreate, self).get_context_data(form=form)
        context['series'] = form.initial['series']
        return context

    def get_success_url(self):
        return self.object.series.get_absolute_url()

class DocumentUpload(generic.UpdateView):
    model = Document
    form_class = forms.UploadForm
    template_name = "translator/document_upform.html"

    def get_success_url(self):
        return self.object.series.get_absolute_url()

class DocumentReserve(generic.View):
    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(Document, pk = kwargs.get('pk'))
        lang = get_object_or_404(Language, code = kwargs.get('code'))
        trans = doc.translation(lang)
        if trans is None:
            trans = Translation.objects.create(doc=doc, lang=lang, untrans=doc.total_str)
        if kwargs.get('action') == 'on' and not trans.reserved_by:
            trans.reserved_by = request.user
        elif kwargs.get('action') == 'off' and trans.reserved_by:
            trans.reserved_by = None
        trans.save()
        success_url = reverse('document_detail', args=[doc.series.uuid, doc.pk, lang.code])
        return HttpResponse(success_url)

class DocumentDelete(generic.View):
    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(Document, pk = kwargs.get('pk'))
        if not doc.series.is_admin(request.user):
            return HttpResponseForbidden()
        success_url = doc.series.get_absolute_url()
        doc.delete()
        messages.success(request, 'The document has been successfully deleted.')
        return HttpResponse(success_url)

class TranslationUpload(generic.FormView):
    model = Translation
    form_class = forms.PoUploadForm
    template_name = "translator/document_upform.html"

    def form_valid(self, form):
        """ kwargs['trans'] is either a Translation model id or a Language code for a new translation """
        try:
            pk = int(self.kwargs['trans'])
            form.instance = self.model.objects.get(pk=pk)
        except ValueError:
            lang_code = self.kwargs['trans']
            form.instance = self.model(
                doc=Document.objects.get(pk=self.kwargs['doc_pk']),
                lang=Language.objects.get(code=lang_code))
        self.lang = form.instance.lang
        self.object = form.save()
        return super(TranslationUpload, self).form_valid(form)

    def get_success_url(self):
        return reverse('document_detail', args=[self.object.doc.series.uuid, self.object.doc.id, self.lang.code])

class TransImageUpload(generic.FormView):
    form_class = forms.UploadForm
    template_name = "translator/document_upform.html"

    def form_valid(self, form):
        pk = int(self.kwargs['pk'])
        lang = get_object_or_404(Language, code=self.kwargs['code'])
        form.instance, created = ImageTranslation.objects.get_or_create(image__pk=pk, lang=lang,
            defaults = {'image_id': int(self.kwargs['pk']), 'lang': lang})
        self.object = form.save()
        return super(TransImageUpload, self).form_valid(form)

    def get_success_url(self):
        return reverse('upload_success', args=[self.object.pk])

class UploadSuccessView(generic.TemplateView):
    template_name="translator/upload_success.html"

    def get_context_data(self, *args, **kwargs):
        context = super(UploadSuccessView, self).get_context_data(*args, **kwargs)
        img_trans = get_object_or_404(ImageTranslation, pk=kwargs.get('doc_pk'))
        context['img_id'] = img_trans.image.pk
        img_trans.image.translation = img_trans
        context['data'] = as_thumbnail(img_trans.image)
        return context

class DocumentImageView(generic.DetailView):
    model = DocumentImage
    context_object_name = 'img'
    template_name = "translator/document_image.html"

    def get_context_data(self, *args, **kwargs):
        context = super(DocumentImageView, self).get_context_data(*args, **kwargs)
        lang = get_object_or_404(Language, code=self.kwargs['code'])
        try:
            context['img'].translation = ImageTranslation.objects.get(lang=lang, image=self.object)
        except ImageTranslation.DoesNotExist:
            context['img'].translation = None
        context['upload_link'] = reverse(
            'image_upload',
            args=[self.object.doc.series.uuid, self.object.doc.pk, lang.code, self.object.pk]
        )
        context['translator'] = self.object.doc.series.is_translator(self.request.user)
        return context

