# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
#from django.views.generic import TemplateView

from translator import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(template_name="index.html"), name='home'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.urls')),

    url(r'^series/new/$', views.SeriesCreate.as_view(), name='series_new'),
    url(r'^series/(?P<slug>[\w\-]+)/$', views.SeriesDetail.as_view(), name='series_detail'),
    url(r'^series/(?P<slug>[\w\-]+)/perms/$', views.SeriesPermsView.as_view(), name='series_perms'),
    url(r'^series/(?P<slug>[\w\-]+)/lang/new/$', views.LanguageAdd.as_view(), name='language_add'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/new/$', views.DocumentCreate.as_view(), name='document_new'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<pk>\d+)/upload/$',
        views.DocumentUpload.as_view(), name='document_upload'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<pk>\d+)/delete/$',
        views.DocumentDelete.as_view(), name='document_delete'),

    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<pk>\d+)/lang/(?P<code>\w+)/$',
        views.DocumentDetail.as_view(), name='document_detail'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<pk>\d+)/lang/(?P<code>\w+)/reserve/(?P<action>(on|off))/$',
        views.DocumentReserve.as_view(), name='document_reserve'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<doc_pk>\d+)/trans/(?P<trans>\w+)/upload/$',
        views.TranslationUpload.as_view(), name='translation_upload'),
    url(r'^series/(?P<slug>[\w\-]+)/doc/(?P<doc_pk>\d+)/lang/(?P<code>\w+)/img/(?P<pk>\d+)/upload/$',
        views.TransImageUpload.as_view(), name='image_upload'),
    url(r'^series/(?P<slug>[\w\-]+)/doc_image/(?P<pk>\d+)/lang/(?P<code>\w+)/$',
        views.DocumentImageView.as_view(), name='document_image'),

    url(r'^upload_success/(?P<doc_pk>\d+)/$', views.UploadSuccessView.as_view(), name='upload_success'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
